﻿namespace CloudScrapeAPI.DTOs
{
    /// <summary>
    /// Cloud Scrape Execution DTO class
    /// </summary>
    public class CloudScrapeExecutionDTO
    {
        public const string QUEUED = "QUEUED";
        public const string PENDING = "PENDING";
        public const string RUNNING = "RUNNING";
        public const string FAILED = "FAILED";
        public const string STOPPED = "STOPPED";
        public const string OK = "OK";

        /// <summary>
        /// The ID of the execution
        /// </summary>
        public string _id;

        /// <summary>
        /// State of the executions. See const definitions on class to see options
        /// </summary>
        public string state;

        /// <summary>
        /// Time the executions was started - in milliseconds since unix epoch
        /// </summary>
        public string starts;

        /// <summary>
        /// Time the executions finished - in milliseconds since unix epoch.
        /// </summary>
        public string finished;

    }
}
