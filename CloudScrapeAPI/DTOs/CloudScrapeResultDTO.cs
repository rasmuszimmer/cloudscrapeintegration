﻿namespace CloudScrapeAPI.DTOs
{
    /// <summary>
    /// Cloud Scrape Result DTO
    /// </summary>
    public class CloudScrapeResultDTO
    {
        /**
         * Header fields
         * @var string[]
         */
        public string[] headers;

        /**
         * An array of arrays containing each row - with each value inside it.
         * @var mixed[][]
         */
        public string[][] rows;

        /**
         * Total number of rows available
         * @var int
         */
        public int totalRows;
    }
}
