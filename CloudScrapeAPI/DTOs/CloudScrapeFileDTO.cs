﻿namespace CloudScrapeAPI.DTOs
{
    /// <summary>
    /// Cloud Scrape File DTO
    /// </summary>
    public class CloudScrapeFileDTO
    {
        /// <summary>
        /// The type of file
        /// </summary>
        public string mimeType;

        /// <summary>
        /// The contents of the file
        /// </summary>
        public string contents;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mimeType"></param>
        /// <param name="contents"></param>
        public CloudScrapeFileDTO(string mimeType, string contents)
        {
            this.mimeType = mimeType;
            this.contents = contents;
        }
    }
}
