﻿namespace CloudScrapeAPI.DTOs
{
    /// <summary>
    /// Cloud Scrape Execution List DTO
    /// </summary>
    public class CloudScrapeExecutionListDTO
    {
        /// <summary>
        /// off set
        /// </summary>
        public int offset;

        /// <summary>
        /// total rows
        /// </summary>
        public int totalRows;

        /// <summary>
        /// array of rows
        /// </summary>
        public CloudScrapeExecutionDTO[] rows;
    }
}
