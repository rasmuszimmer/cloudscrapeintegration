﻿namespace CloudScrapeAPI.DTOs
{
    /// <summary>
    /// Cloud Scrape Run DTO
    /// </summary>
    public class CloudScrapeRunDTO
    {
        /// <summary>
        /// The ID of the run
        /// </summary>
        public string _id;

        /// <summary>
        /// Name of the run
        /// </summary>
        public string name;
    }
}
