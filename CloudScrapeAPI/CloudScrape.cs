﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using CloudScrapeAPI.DTOs;

namespace CloudScrapeAPI
{
    /// <summary>
    /// Cloud Response details
    /// </summary>
    public class CloudResponse
    {
        public string Content { get; set; }
        public Stream ResponseStream { get; set; }
        public WebHeaderCollection Headers { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string StatusDescription { get; set; }
    }

    /// <summary>
    /// Cloud Scrape class
    /// </summary>
    public class CloudScrape
    {

        private CloudScrapeClient _client;

        /// <summary>
        /// Initial method 
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="accountId"></param>
        public void Init(string apiKey, string accountId)
        {
            _client = new CloudScrapeClient(apiKey, accountId);
        }

        /// <summary>
        /// Default Client
        /// </summary>
        /// <returns></returns>
        public CloudScrapeClient DefaultClient()
        {
            CheckState();

            return _client;
        }

        /// <summary>
        /// Execute Method
        /// </summary>
        /// <returns></returns>
        public CloudScrapeExecutions Executions()
        {
            CheckState();

            return _client.Executions();
        }

        /// <summary>
        /// Run Method
        /// </summary>
        /// <returns></returns>
        public CloudScrapeRuns Runs()
        {
            CheckState();

            return _client.Runs();
        }

        /// <summary>
        /// Check State
        /// </summary>
        private void CheckState()
        {
            if (_client == null)
            {
                throw new Exception("You must call init first before using the API");
            }
        }
    }

    /// <summary>
    /// Cloud Scrape Client
    /// </summary>
    public class CloudScrapeClient
    {
        private readonly string _accountId;
        private readonly string _accessKey;

        private string _endPoint = "https://app.cloudscrape.com/api/";
        private string _userAgent = "CS-ASP-CLIENT/1.0";
        private int _requestTimeout = 60*60*1000;

        private readonly CloudScrapeExecutions _objExecutions;
        private readonly CloudScrapeRuns _objRuns;

        /// <summary>
        /// Endpoint / base url of requests
        /// </summary>
        public string EndPoint
        {
            get { return _endPoint; }
            set { _endPoint = value; }
        }

        /// <summary>
        /// User agent of requests
        /// </summary>
        public string UserAgent
        {
            get { return _userAgent; }
            set { _userAgent = value; }
        }

        /// <summary>
        /// Set request timeout. Defaults to 1 hour
        /// Note: If you are using the sync methods and some requests are running for very long you need to increase this value.
        /// </summary>
        public int RequestTimeout
        {
            get { return _requestTimeout; }
            set { _requestTimeout = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="accountId"></param>
        public CloudScrapeClient(string apiKey, string accountId)
        {
            this._accountId = accountId;
            this._accessKey = CreateMD5(_accountId + apiKey).ToLower(); 
            this._objExecutions = new CloudScrapeExecutions(this);
            this._objRuns = new CloudScrapeRuns(this);
        }

        public void DownloadFileRequest(string url, string localFileName)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("X-CloudScrape-Access", _accessKey);
            webClient.Headers.Add("X-CloudScrape-Account", _accountId);
            webClient.DownloadFile(EndPoint + url, localFileName);
        }

        /// <summary>
        /// Make a call to the CloudScrape API
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public CloudResponse Request(string url, string method = "GET", string body = null)
        {
            var req = System.Net.HttpWebRequest.Create(EndPoint + url) as HttpWebRequest;

            req.Headers.Add("X-CloudScrape-Access", _accessKey);
            req.Headers.Add("X-CloudScrape-Account", _accountId);
            req.UserAgent = _userAgent;
            req.Timeout = _requestTimeout;
            req.Accept = "application/json";
            req.ContentType = "application/json";
            req.Method = method;

            //Fix 3: body is not written to request stream
            if (body != null)
            {
                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    streamWriter.Write(body);
                    streamWriter.Flush();
                }
            }

            CloudResponse objCloudResponse = null;
            HttpWebResponse response = null;
            StreamReader readStream = null;

            try
            {

                response = (HttpWebResponse)req.GetResponse();

                objCloudResponse = new CloudResponse
                {
                    StatusCode = response.StatusCode,
                    Headers = response.Headers,
                    StatusDescription = response.StatusDescription
                };

                WebHeaderCollection obj = response.Headers;
                Stream receiveStream = response.GetResponseStream();
                objCloudResponse.ResponseStream = receiveStream;

                readStream = new StreamReader(receiveStream, Encoding.UTF8);
                objCloudResponse.Content = readStream.ReadToEnd();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }

                if (readStream != null)
                {
                    readStream.Close();
                }
            }

            return objCloudResponse;
        }

        /// <summary>
        /// MD5 Encryption
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            var sb = new StringBuilder();
            foreach (var t in hashBytes)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Execute request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public string RequestJson(string url, string method = "GET", string body = null)
        {
            CloudResponse response = this.Request(url, method, body);
            return response.Content;
        }

        /// <summary>
        /// Get response boolean value
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public bool RequestBoolean(string url, string method = "GET", string body = null)
        {
            this.Request(url, method, body);
            return true;
        }

        /// <summary>
        /// Interact with executions.
        /// </summary>
        /// <returns></returns>
        public CloudScrapeExecutions Executions()
        {
            return this._objExecutions;
        }

        /// <summary>
        /// Interact with runs
        /// </summary>
        /// <returns></returns>
        public CloudScrapeRuns Runs()
        {
            return this._objRuns;
        }

    }

    /// <summary>
    /// Execute Cloud Scrape request
    /// </summary>
    public class CloudScrapeExecutions
    {

        private CloudScrapeClient client;
        private JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

        public CloudScrapeExecutions(CloudScrapeClient client)
        {
            this.client = client;
        }

        /// <summary>
        /// Get execution
        /// </summary>
        /// <param name="executionId"></param>
        /// <returns></returns>
        public CloudScrapeExecutionDTO Get(string executionId)
        {
            string strResponse = this.client.RequestJson("executions/" + executionId);

            var result = jsonSerializer.Deserialize<CloudScrapeExecutionDTO>(strResponse);
            return result;
        }

        /// <summary>
        /// Delete execution permanently
        /// </summary>
        /// <param name="executionId"></param>
        /// <returns></returns>
        public bool Remove(string executionId)
        {
            return this.client.RequestBoolean("executions/" + executionId, "DELETE");
        }

        /// <summary>
        /// Get the entire result of an execution.
        /// </summary>
        /// <param name="executionId"></param>
        /// <returns></returns>
        public CloudScrapeResultDTO GetResult(string executionId)
        {
            string strResponse = this.client.RequestJson("executions/" + executionId + "/result");
            var result = jsonSerializer.Deserialize<CloudScrapeResultDTO>(strResponse);
            return result;
        }

        /// <summary>
        /// Get a file from a result set
        /// </summary>
        /// <param name="executionId"></param>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public CloudScrapeFileDTO GetResultFile(string executionId, string fileId)
        {
            var response = this.client.Request("executions/" + executionId + "/file/" + fileId);
            return new CloudScrapeFileDTO(response.Headers["Content-Type"], response.Content);
        }

        public void DownloadFile(string executionId, string fileId, string localFilename)
        {
            this.client.DownloadFileRequest("executions/" + executionId + "/file/" + fileId, localFilename);
        }

        /// <summary>
        /// Stop running execution
        /// </summary>
        /// <param name="executionId"></param>
        /// <returns></returns>
        public bool Stop(string executionId)
        {
            return this.client.RequestBoolean("executions/" + executionId + "/stop", "POST");
        }

        /// <summary>
        /// Resume stopped execution
        /// </summary>
        /// <param name="executionId"></param>
        /// <returns></returns>
        public bool Resume(string executionId)
        {
            return this.client.RequestBoolean("executions/" + executionId + "/continue", "POST");
        }
    }

    /// <summary>
    /// Run Cloud Scrape robots
    /// </summary>
    public class CloudScrapeRuns
    {

        private CloudScrapeClient client;
        private JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="client"></param>
        public CloudScrapeRuns(CloudScrapeClient client)
        {
            this.client = client;
        }

        /// <summary>
        /// Get Run Detail
        /// </summary>
        /// <param name="runId"></param>
        /// <returns></returns>
        public CloudScrapeRunDTO Get(string runId)
        {

            string strResponse = this.client.RequestJson("runs/" + runId);
            var result = jsonSerializer.Deserialize<CloudScrapeRunDTO>(strResponse);
            return result;
        }

        /// <summary>
        /// Permanently delete run
        /// </summary>
        /// <param name="runId"></param>
        /// <returns></returns>
        public bool Remove(string runId)
        {
            return this.client.RequestBoolean("runs/" + runId, "DELETE");
        }

        /// <summary>
        /// Start new execution of the run
        /// </summary>
        /// <param name="runId"></param>
        /// <returns></returns>
        public CloudScrapeExecutionDTO Execute(string runId)
        {
            string strResponse = this.client.RequestJson("runs/" + runId + "/execute", "POST");
            var result = jsonSerializer.Deserialize<CloudScrapeExecutionDTO>(strResponse);
            return result;
        }

        /// <summary>
        ///  Start new execution of the run, and wait for it to finish before returning the result.
        ///  The execution and result will be automatically deleted from CloudScrape completion
        ///  both successful and failed.
        /// </summary>
        /// <param name="runId"></param>
        /// <returns></returns>
        public CloudScrapeResultDTO ExecuteSync(string runId)
        {
            string strResponse = this.client.RequestJson("runs/" + runId + "/execute/wait", "POST");
            var result = jsonSerializer.Deserialize<CloudScrapeResultDTO>(strResponse);
            return result;
        }

        /// <summary>
        /// Starts new execution of run with given inputs
        /// </summary>
        /// <param name="runId"></param>
        /// <param name="inputs"></param>
        /// <returns></returns>
        public CloudScrapeExecutionDTO ExecuteWithInput(string runId, string inputs)
        {
            string strResponse = this.client.RequestJson("runs/" + runId + "/execute/inputs", "POST", inputs);
            var result = jsonSerializer.Deserialize<CloudScrapeExecutionDTO>(strResponse);
            return result;
        }

        /// <summary>
        /// Starts new execution of run with given inputs, and wait for it to finish before returning the result.
        /// The inputs, execution and result will be automatically deleted from CloudScrape upon completion - both successful and failed.
        /// </summary>
        /// <param name="runId"></param>
        /// <param name="inputs"></param>
        /// <returns></returns>
        public CloudScrapeResultDTO ExecuteWithInputSync(string runId, string inputs)
        {
            
            var strResponse = this.client.RequestJson("runs/" + runId + "/execute/inputs/wait", "POST", inputs);
            
            
            var result = jsonSerializer.Deserialize<CloudScrapeResultDTO>(strResponse);
            return result;
        }

        /// <summary>
        /// Get the result from the latest execution of the given run.
        /// </summary>
        /// <param name="runId"></param>
        /// <returns></returns>
        public CloudScrapeResultDTO GetLatestResult(string runId)
        {
            string strResponse = this.client.RequestJson("runs/" + runId + "/latest/result");
            var result = jsonSerializer.Deserialize<CloudScrapeResultDTO>(strResponse);
            return result;
        }

        /// <summary>
        /// Get executions for the given run.
        /// </summary>
        /// <param name="runId"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public CloudScrapeExecutionListDTO GetExecutions(string runId, int offset = 0, int limit = 30)
        {
            string strResponse = this.client.RequestJson("runs/" + runId + "/executions?offset=" + offset + "&limit=" + limit);
            var result = jsonSerializer.Deserialize<CloudScrapeExecutionListDTO>(strResponse);
            return result;
        }
    }
}