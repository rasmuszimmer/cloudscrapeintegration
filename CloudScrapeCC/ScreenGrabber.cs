﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using CloudScrapeAPI;
using CloudScrapeAPI.DTOs;
using CloudScrapeCC.Adapters;

namespace CloudScrapeCC
{
    internal class ScreenGrabber : Robot
    {
        private readonly CloudScrapeRuns _runsApi;
        private readonly CloudScrapeExecutions _executionsApi;
        private Timer _timer;

        internal ScreenGrabber(CloudScrapeRuns runsApi, CloudScrapeExecutions executionsApi)
        {
            _runsApi = runsApi;
            _executionsApi = executionsApi;
        }

        private void PollExecution(string executionId, Adapter adapter)
        {
            var executions = _executionsApi.Get(executionId);

            if (executions.state != CloudScrapeExecutionDTO.OK)
            {
                return;
            }
            
            _timer.Dispose();

            var result = _executionsApi.GetResult(executionId);

            var ssIndices = adapter.GetScreenShotIndices();

            try
            {
                foreach (var i in ssIndices)
                {
                    var header = result.headers[i];
                    var ssDataRow = result.rows[0][i];
                    var fileId = ssDataRow.Split(";".ToCharArray())[2];
                    var localFilename = "C:\\" + header + ".png";
                    _executionsApi.DownloadFile(executionId, fileId, localFilename);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            finally
            {
                Environment.Exit(-1);
            }
        }

        public override void Execute(string runId, string filterId, string adapterName)
        {
            var sc = ExecuteGetCriterias(filterId);

            // Resolve adapter
            var adapter = GetAdapter(adapterName, filterId);

            // Grab middle criteria
            var inputs = adapter.AdaptInput(sc.GetRange(sc.Count/2, 1)).FirstOrDefault();

            var result = new CloudScrapeExecutionDTO();
            try
            {
                result = _runsApi.ExecuteWithInput(runId, inputs);
                _timer = new Timer(
                e => PollExecution(result._id, adapter),
                null,
                TimeSpan.FromSeconds(20),
                TimeSpan.FromSeconds(20));
            }
            catch (WebException webEx)
            {
                Log.Fatal(webEx.Message);
            }
        }
    }
}
