﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudScrapeAPI;
using DAL.ContentExpress;
using DAL.CSC;
using NLog;

namespace CloudScrapeCC
{
    internal class RobotRunnerFactory : Factory
    {
        internal override Robot Create()
        {
            Log.Debug("Creating RobotRunner");

            return new RobotRunner(new ContentExpressRepository(), CloudScrape.Runs());
        }
    }
}
