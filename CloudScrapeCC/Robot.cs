﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudScrapeCC.Adapters;
using DAL.CSC;
using NLog;

namespace CloudScrapeCC
{
    internal abstract class Robot
    {
        protected Logger Log { get; private set; }
        protected readonly ICSCRepository _cscRepository;

        protected Robot()
        {
            _cscRepository = new CSCRepository();
            Log = LogManager.GetLogger(GetType().FullName);
        }

        protected List<SearchCriteria> ExecuteGetCriterias(string filterId)
        {
            var sc = new List<SearchCriteria>();
            try
            {
                sc = _cscRepository.GetSearchCriteria(Int32.Parse(filterId));
            }
            catch (SqlException ex)
            {
                //if(ex.ErrorCode)
                ExecuteGetCriterias(filterId);
            }

            return sc;
        }

        protected Adapter GetAdapter(string adapterName, string filterId)
        {
            Log.Debug("Resolving adapter: {0}", adapterName);
            var adapterType = (from asm in AppDomain.CurrentDomain.GetAssemblies()
                               from type in asm.GetTypes()
                               where type.IsClass && type.Name == adapterName
                               select type).Single();
            return (Adapter)Activator.CreateInstance(adapterType, filterId);
        }

        public abstract void Execute(string runId, string filterId, string adapterName);
    }
}
