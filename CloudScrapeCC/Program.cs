﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudScrapeAPI;
using CloudScrapeCC.Adapters;
using DAL.ContentExpress;
using DAL.CSC;
using NLog;

namespace CloudScrapeCC
{
    class Program
    {
        private static void Main(string[] args)
        {
            var skyjetFilterId = "10040100";
            var skyjetRunId = "33ed9740-c9bb-4513-a5ec-a58a09e485d1";
            var skyjetAdapterName = "SkyjetOw";
            var travelAirFilterId = "10070295";
            var travelAirRunId = "7615b440-e0c2-404a-8a7f-31ffde0cb7e3";
            var travelAirAdapterName = "TravelAirOw";
            var deltaFilterId = "145055";
            var deltaRunId = "056f5ddf-0a79-412a-a787-a33967b373ac";
            var deltaAdapterName = "DeltaOw";

            // Test args
            //args = new[] { deltaRunId, deltaAdapterName, deltaFilterId};
            var runId = args[0];
            var adapterName = args[1] + "Adapter";
            var filterId = args[2];

            //var robot = RobotRunnerFactory.CreateRobotRunner();
            //robot.ExecuteRun(runId, filterId, adapterName);

            var robot = new ScreenGrabberFactory().Create();
            robot.Execute(runId, filterId, adapterName);

            Console.ReadLine();
        }
    }
}
