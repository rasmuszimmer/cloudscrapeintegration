﻿using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using DAL.ContentExpress;
using DAL.CSC;
using NLog;

namespace CloudScrapeCC.Adapters
{
    public abstract class Adapter
    {
        private readonly JavaScriptSerializer _serializer;
        protected static readonly string SlaveTablePrefix = "T_air_data_rzr_";
        protected string FilterId;
        protected Logger Log { get; private set; }

        protected Adapter(string filterId)
        {
            FilterId = filterId;
            _serializer = new JavaScriptSerializer();
            Log = LogManager.GetLogger(GetType().FullName);
        }

        protected Adapter()
        {
        }

        protected string Serialize(object obj)
        {
           return  _serializer.Serialize(obj);
        }

        public void CheckForNoNullsAllowedException(string[] row)
        {
            // Step back through the result - skipping the error column
            // to check for nulls (only the error column can be null)
            for (var i = row.Length - 2; i >= 0; i--)
            {
                if (row[i] == null)
                {
                    throw new NoNullAllowedException(
                        "No rows can be null in a result row (except for the error column) - this error can be due to a manual cancellation of the execution");
                }
            }
        }

        public SearchCriteria CriteriaSample { get; set; }

        public abstract bool HasAdapterSpecificError(string row);

        public abstract IEnumerable<string> AdaptInput(List<SearchCriteria> criteria);

        public abstract List<int> GetScreenShotIndices();  

        public abstract List<AirDataRow> AdaptResultRows(string[] row);

        public abstract bool IsNoFlightAvailable(string[] row);

        public abstract bool IsSoldOut(string[] row);

    }
}
