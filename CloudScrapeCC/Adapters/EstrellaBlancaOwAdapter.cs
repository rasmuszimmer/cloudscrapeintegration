﻿using System;
using System.Collections.Generic;
using DAL.ContentExpress;
using DAL.CSC;

namespace CloudScrapeCC.Adapters
{
    class EstrellaBlancaOwAdapter : Adapter
    {
        public EstrellaBlancaOwAdapter(string filterId)
            : base(filterId)
        {
        }

        public override bool HasAdapterSpecificError(string row)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<string> AdaptInput(List<SearchCriteria> criteria)
        {
            throw new NotImplementedException();
        }

        public override List<int> GetScreenShotIndices()
        {
            throw new NotImplementedException();
        }

        public override List<AirDataRow> AdaptResultRows(string[] row)
        {
            throw new NotImplementedException();
        }

        public override bool IsNoFlightAvailable(string[] row)
        {
            throw new NotImplementedException();
        }

        public override bool IsSoldOut(string[] row)
        {
            throw new NotImplementedException();
        }
    }
}
