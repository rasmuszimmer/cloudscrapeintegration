﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using DAL.ContentExpress;
using DAL.CSC;
using NLog;

namespace CloudScrapeCC.Adapters.AwanSqivaCom
{
    public abstract class AwanSqivaAdapter : Adapter
    {
        protected string SlaveTableName = SlaveTablePrefix + "AwanSqiva_";
        private readonly AwanSqivaOutputFormatter _outputFormatter;
        private readonly AwanSqivaInputFormatter _inputFormatter;

        protected AwanSqivaAdapter(string filterId) : base(filterId)
        {
            _outputFormatter = new AwanSqivaOutputFormatter();
            _inputFormatter = new AwanSqivaInputFormatter();
        }

        protected AwanSqivaAdapter()
        {
        }

        protected void SetTableNameSuffix(string suffix)
        {
            SlaveTableName += suffix;
        }

        public override IEnumerable<string> AdaptInput(List<SearchCriteria> criteria)
        {
            CriteriaSample = criteria.First();

            Log.Trace("Converting and serializing {0} criteria for OD {1}-{2}", criteria.Count, CriteriaSample.Origin, CriteriaSample.Destination);

            return criteria.Select(instance => new InputLineDTO()
            {
                origin = _inputFormatter.FormatOrigin(instance.Origin),
                destination = _inputFormatter.FormatDestination(instance.Destination),
                departureDay = _inputFormatter.FormatDay(instance.OutDay),
                departureMonth = _inputFormatter.FormatMonth(instance.OutMonth),
                departureYear = _inputFormatter.FormatYear(instance.OutYear)
            }).Select(Serialize);
        }

        public override List<AirDataRow> AdaptResultRows(string[] row)
        {
            var adrList = new List<AirDataRow>();
            var adr = new AirDataRow()
            {
                TableName = SlaveTableName,
                JobId = FilterId,
                Pos = CriteriaSample.Pos,
                Origin = row[0],
                Destination = row[1],
                Carrier = _outputFormatter.FormatCarrier(row[5]),
                FirstFlightNo = _outputFormatter.FormatFlightNo(row[5]),
                BptFirstTravelDep = _outputFormatter.FormatBpt(row[2], row[3], row[4], row[6]),
                BptFirstTravelArr = _outputFormatter.FormatBpt(row[2], row[3], row[4], row[7]),
                IsOneWay = true,
                Price = _outputFormatter.FormatPrice(row[8]),
                Currency = _outputFormatter.FormatCurrency(row[8]),
                // For this adapter, tax is not actually included
                IsTaxIncluded = true,
                BookingClassFirst = "E",
                FareType = CriteriaSample.Cabin
            };

            adr.FareBasisFirst = _outputFormatter.FormatFareBasis(adr.Price, adr.Carrier);
            adrList.Add(adr);

            return adrList;
        }

        public class AwanSqivaOutputFormatter
        {
            private static readonly Regex PriceMatch = new Regex(@"\d[\d\,\.]*");
            private static readonly Regex NoDash = new Regex(@"[-]");

            public string FormatFareBasis(double price, string carrier)
            {
                return Convert.ToInt32(price) + carrier;
            }

            public string FormatCarrier(string s)
            {
                var tokens = s.Split("-".ToCharArray());
                return tokens[0];
            }

            public string FormatFlightNo(string s)
            {
                return NoDash.Replace(s, "");
            }

            public int FormatBpt(string day, string month, string year, string time)
            {
                var y = Int32.Parse(year);

                // Add one to reverse the formatting done in the input adaption step
                var m = Int32.Parse(month) + 1;
                var d = Int32.Parse(day);
                var extractedTime = ExtractTime(time);
                var hourComponent = extractedTime[0];
                var minuteComponent = extractedTime[1];
                var reconstructedDate = new DateTime(y, m, d, hourComponent, minuteComponent, 0);

                return Utils.DateTime2BPT(reconstructedDate);
            }

            // Returns int[2]{hours, minutes}
            public int[] ExtractTime(string s)
            {
                var tokens = s.Split(" ".ToCharArray());
                var time = tokens[0];
                var timeComponentLen = time.Length;
                var half = timeComponentLen / 2;
                var hourComponent = Int32.Parse(time.Substring(0, half));
                var minuteComponent = Int32.Parse(time.Substring(half, half));

                return new[] { hourComponent, minuteComponent };
            }

            public double FormatPrice(string s)
            {
                var priceStr = PriceMatch.Match(s).Value;
                var parsed = Double.Parse(priceStr, CultureInfo.InvariantCulture);
                return parsed;
            }

            public string FormatCurrency(string s)
            {
                var tokens = s.Split(" ".ToCharArray());
                return tokens[0];
            }
        }

        public class AwanSqivaInputFormatter
        {
            public string FormatOrigin(string origin)
            {
                return origin;
            }

            public string FormatDestination(string destination)
            {
                return destination;
            }

            public string FormatDay(string day)
            {
                var parsed = Int32.Parse(day);
                return IntAsInvariantCultureString(parsed);
            }

            public string FormatMonth(string month)
            {
                // This deduction is specific to the Skyjet adapter since
                // the 12 months of input in the scraper are 0-based
                var parsed = Int32.Parse(month);
                return IntAsInvariantCultureString(parsed - 1);
            }

            public string FormatYear(string year)
            {
                return year;
            }

            private static string IntAsInvariantCultureString(int i)
            {
                return i.ToString(CultureInfo.InvariantCulture);
            }
        }

        protected class InputLineDTO
        {
            public string origin { get; set; }
            public string destination { get; set; }
            public string departureDay { get; set; }
            public string departureMonth { get; set; }
            public string departureYear { get; set; }
        }
    }
}
