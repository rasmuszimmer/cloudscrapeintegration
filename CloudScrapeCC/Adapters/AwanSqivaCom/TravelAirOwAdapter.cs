﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DAL.ContentExpress;
using DAL.CSC;

namespace CloudScrapeCC.Adapters.AwanSqivaCom
{
    public class TravelAirOwAdapter : AwanSqivaAdapter
    {
        private const string TableSuffix = "TravelAir_OW";
        private static TravelAirOwInputFormatter _inputFormatter;
        private static TravelAirOwOutputFormatter _outputFormatter; 

        public TravelAirOwAdapter(string filterId)
            : base(filterId)
        {
            SetTableNameSuffix(TableSuffix);
            _inputFormatter = new TravelAirOwInputFormatter();
            _outputFormatter = new TravelAirOwOutputFormatter();
        }

        public TravelAirOwAdapter()
        {
        }

        public override bool HasAdapterSpecificError(string error)
        {
            return error.Equals("Assertion failed - element not found");
        }

        public override bool IsNoFlightAvailable(string[] row)
        {
            return row[4].Equals("Flight No.") 
                && row[5].Equals("Depart") 
                && row[6].Equals("Arrival") 
                && row[7].Equals("Fare");
        }

        public override bool IsSoldOut(string[] row)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<string> AdaptInput(List<SearchCriteria> criteria)
        {
            CriteriaSample = criteria.First();

            return criteria.Select(instance => new InputLineDTO()
            {
                origin = _inputFormatter.FormatOrigin(instance.Origin),
                destination = _inputFormatter.FormatDestination(instance.Destination),
                departureDay = _inputFormatter.FormatDay(instance.OutDay),
                departureMonthAndYear = _inputFormatter.FormatMonthYear(instance.OutMonth, instance.OutYear)
            }).Select(Serialize);
        }

        public override List<int> GetScreenShotIndices()
        {
            throw new NotImplementedException();
        }

        public override List<AirDataRow> AdaptResultRows(string[] row)
        {
            var adrList = new List<AirDataRow>();
            var adr = new AirDataRow()
            {
                TableName = SlaveTableName,
                JobId = FilterId,
                Pos = CriteriaSample.Pos,
                Origin = row[0],
                Destination = row[1],
                Carrier = _outputFormatter.FormatCarrier(row[4]),
                FirstFlightNo = _outputFormatter.FormatFlightNo(row[4]),
                BptFirstTravelDep = _outputFormatter.FormatBpt(row[3], row[2], row[5]),
                BptFirstTravelArr = _outputFormatter.FormatBpt(row[3], row[2], row[6]),
                IsOneWay = true,
                Price = _outputFormatter.FormatPrice(row[7]),
                Currency = _outputFormatter.FormatCurrency(row[7]),
                // For this adapter, tax is not actually included
                IsTaxIncluded = true,
                // Looks like the insert maps maxstay to instanceId
                MaxStay = "283499",
                BookingClassFirst = "E",
                FareType = CriteriaSample.Cabin
            };

            adr.FareBasisFirst = _outputFormatter.FormatFareBasis(adr.Price, adr.Carrier);
            adrList.Add(adr);

            return adrList;
        }

        private class TravelAirOwInputFormatter : AwanSqivaInputFormatter
        {
            public string FormatMonthYear(string month, string year)
            {
                var parsedMonth = int.Parse(month);
                var abbrMonth = CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat.GetAbbreviatedMonthName(parsedMonth).ToUpper();
                return string.Format("{0} {1}", abbrMonth, year);
            } 
        }

        private class TravelAirOwOutputFormatter : AwanSqivaOutputFormatter
        {
            public int FormatBpt(string day, string monthAndYear, string time)
            {
                var culture = CultureInfo.CreateSpecificCulture("en-US");
                
                var d = int.Parse(day);
                var mny = monthAndYear.Split(" ".ToCharArray());
                var m = culture.TextInfo.ToTitleCase(mny[0].ToLower());
                var y = int.Parse(mny[1]);
                var monthNames = culture.DateTimeFormat.AbbreviatedMonthNames;
                var monthNumber = Array.IndexOf(monthNames, m) + 1;
                var extractedTime = ExtractTime(time);
                var hourComponent = extractedTime[0];
                var minuteComponent = extractedTime[1];
                var reconstructedDate = new DateTime(y, monthNumber, d, hourComponent, minuteComponent, 0);

                return Utils.DateTime2BPT(reconstructedDate);
            }
        }

        private new class InputLineDTO
        {
            public string origin { get; set; }
            public string destination { get; set; }
            public string departureDay { get; set; }
            public string departureMonthAndYear { get; set; }
        }
    }
}
