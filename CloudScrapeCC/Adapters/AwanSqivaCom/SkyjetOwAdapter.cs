﻿using System.Collections.Generic;
using DAL.CSC;

namespace CloudScrapeCC.Adapters.AwanSqivaCom
{
    public class SkyjetOwAdapter : AwanSqivaAdapter
    {
        private const string TableSuffix = "Skyjet_OW";
        private static SkyjetOwInputFormatter _inputFormatter;


        public SkyjetOwAdapter(string filterId)
            : base(filterId)
        {
            SetTableNameSuffix(TableSuffix);
            _inputFormatter = new SkyjetOwInputFormatter();
        }

        public override List<int> GetScreenShotIndices()
        {
            throw new System.NotImplementedException();
        }

        public override bool IsNoFlightAvailable(string[] row)
        {
            return row[5].Equals("no available flight");
        }

        public override bool IsSoldOut(string[] row)
        {
            throw new System.NotImplementedException();
        }

        public override bool HasAdapterSpecificError(string error)
        {
            return error.Equals("No elements found in loop");
        }

        public class SkyjetOwInputFormatter : AwanSqivaInputFormatter
        {
        }

        public class SkyjetOwOutputFormatter : AwanSqivaOutputFormatter
        {
        }
    }
}
