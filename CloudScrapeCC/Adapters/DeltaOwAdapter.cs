﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudScrapeCC.Adapters.AwanSqivaCom;
using DAL.ContentExpress;
using DAL.CSC;

namespace CloudScrapeCC.Adapters
{
    public class DeltaOwAdapter : Adapter
    {
        private readonly string SlaveTableName = SlaveTablePrefix + "Delta_OW";
        private static DeltaOwInputFormatter _inputFormatter;
        private static DeltaOwOutputFormatter _outputFormatter;

        public DeltaOwAdapter(string filterId)
            : base(filterId)
        {
            _inputFormatter = new DeltaOwInputFormatter();
            _outputFormatter = new DeltaOwOutputFormatter();
        }

        public override bool HasAdapterSpecificError(string row)
        {
            return false;
        }

        public override IEnumerable<string> AdaptInput(List<SearchCriteria> criteria)
        {
            CriteriaSample = criteria.First();

            return criteria.Select(instance => new InputLineDTO()
            {
                origin = instance.Origin,
                destination = instance.Destination,
                departureDate = _inputFormatter.FormatDate(instance.OutDay, instance.OutMonth, instance.OutYear)
            }).Select(Serialize);
        }

        public override List<int> GetScreenShotIndices()
        {
            return new List<int>(){3, 4, 5};
        }

        private AirDataRow AdaptResultRow(string[] row)
        {
            var adr = new AirDataRow()
            {
                TableName = SlaveTableName,
                JobId = FilterId,
                Pos = CriteriaSample.Pos,
                Origin = row[0],
                Destination = row[1],
                Carrier = "DL",
                FirstFlightNo = row[6],
                BptFirstTravelDep = _outputFormatter.FormatBpt(row[2], row[4]),
                BptFirstTravelArr = _outputFormatter.FormatBpt(row[2], row[5]),
                FirstTravelStopOver = row[7],
                IsOneWay = true,
                Currency = "USD",
                // For this adapter, tax is not actually included
                IsTaxIncluded = true,
                // Looks like the insert maps maxstay to instanceId
                MaxStay = "283499",
            };

            return adr;
        }

        public override List<AirDataRow> AdaptResultRows(string[] row)
        {
            var adrList = new List<AirDataRow>();


            // Main cabin
            if (!row[8].Equals("Not Available"))
            {
                var adr = AdaptResultRow(row);
                adr.Price = _outputFormatter.FormatPrice(row[8]);
                adr.BookingClassFirst = CriteriaSample.Cabin;
                adr.FareType = CriteriaSample.Cabin;
                adr.FareBasisFirst = _outputFormatter.FormatFareBasis(adr.Price, adr.Carrier);
                adrList.Add(adr); 
            }

            // Upper class
            if (!row[9].Equals("Not Available"))
            {
                var adr = AdaptResultRow(row);
                adr.Price = _outputFormatter.FormatPrice(row[9]);
                adr.BookingClassFirst = row[10];
                adr.FareType = row[10];
                adr.FareBasisFirst = _outputFormatter.FormatFareBasis(adr.Price, adr.Carrier);
                adrList.Add(adr); 
            }
            

            return adrList;
        }

        public override bool IsNoFlightAvailable(string[] row)
        {
            return false;
        }

        public override bool IsSoldOut(string[] row)
        {
            throw new NotImplementedException();
        }

        private class DeltaOwInputFormatter
        {
            public string FormatDate(string day, string month, string year)
            {
                return month + day + year;
            }
        }

        public class DeltaOwOutputFormatter
        {
            private static CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");
            public int FormatBpt(string date, string time)
            {
                var dtString = string.Format("{0}/{1}/{2} {3}", date.Substring(0, 2), date.Substring(2, 2), date.Substring(4),
                    time);
                //var dtString = date.Substring(0, 2) + " " + time;
                var dt = Convert.ToDateTime(dtString, ci);
                return Utils.DateTime2BPT(dt);
            }

            public double FormatPrice(string s)
            {
                const NumberStyles styles = NumberStyles.Number | NumberStyles.AllowCurrencySymbol;
                var parsed = Double.Parse(s, styles, ci);
                return parsed;
            }

            public string FormatFareBasis(double price, string carrier)
            {
                var toInt = Convert.ToInt32(price);
                return toInt+carrier;
            }
        }

        private class InputLineDTO
        {
            public string origin { get; set; }
            public string destination { get; set; }
            public string departureDate { get; set; }
        }
    }
}
