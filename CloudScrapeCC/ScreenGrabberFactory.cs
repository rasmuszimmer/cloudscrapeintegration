﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudScrapeAPI;
using DAL.ContentExpress;
using DAL.CSC;

namespace CloudScrapeCC
{
    internal class ScreenGrabberFactory : Factory
    {
        internal override Robot Create()
        {
            Log.Debug("Creating RobotRunner");

            return new ScreenGrabber(CloudScrape.Runs(), CloudScrape.Executions());
        }
    }
}
