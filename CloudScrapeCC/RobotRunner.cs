﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CloudScrapeAPI;
using CloudScrapeAPI.DTOs;
using CloudScrapeCC.Adapters;
using DAL.ContentExpress;
using DAL.CSC;
using NLog;

namespace CloudScrapeCC
{
    internal class RobotRunner : Robot
    {
        private readonly IContentExpressRepository _contentExpressRepository;
        
        private readonly CloudScrapeRuns _runsApi;

        private const int MaxRetries = 5;
        private int currentRetriesCount = 0;
        private int rowsInserted = 0;

        internal RobotRunner(IContentExpressRepository contentExpressRepository, CloudScrapeRuns runsApi)
        {
            _contentExpressRepository = contentExpressRepository;
            _runsApi = runsApi;
        }

        public override void Execute(string runId, string filterId, string adapterName)
        {
            // Get searchCriteria
            var sc = ExecuteGetCriterias(filterId);
                //.Where(x => x.AllowedConnections == string.Empty).ToList();

            // Resolve adapter
            var adapter = GetAdapter(adapterName, filterId);

            // Get adapted inputs from searchCriteria 
            var inputs = adapter.AdaptInput(sc);
            
            // Execute robot
            var stopwatch = Stopwatch.StartNew();

            foreach (var inputLine in inputs)
            {
                ExecuteRun(runId, inputLine, adapter);
            }
            
            stopwatch.Stop();
            Log.Info("\nFull run finished in {0} --- inserted {1} fare observations", stopwatch.Elapsed, rowsInserted);
        }

        private void ExecuteRun(string runId, string inputLine, Adapter adapter)
        {
            Log.Info("Executing robot {0} with runId: {1} and input \n{2}", adapter.GetType().FullName, runId, inputLine);
            var stopwatch = Stopwatch.StartNew();

            var result = new CloudScrapeResultDTO();
            try
            {
                result = _runsApi.ExecuteWithInputSync(runId, inputLine);
                currentRetriesCount = 0;
            }
            catch (WebException webEx)
            {
                if (webEx.Status != WebExceptionStatus.Timeout) throw;
                if (currentRetriesCount >= MaxRetries)
                {
                    Log.Fatal("Max no. of retries reached - aborting");
                    throw;
                }

                currentRetriesCount++;
                Log.Trace("Response timeout - retrying ({0})", currentRetriesCount);
                ExecuteRun(runId, inputLine, adapter);
                return;
            }
            stopwatch.Stop();
            Log.Info("ExecuteWithInputSync finished successfully  in {0} and returned: {1} row(s)", stopwatch.Elapsed, result.rows.Length);

            // Something went wrong with request - Retry
            if (result.rows == null)
            {
                Log.Warn("Result was NULL - retrying...");
                ExecuteRun(runId, inputLine, adapter);
                return;
            }

            Log.Debug("Checking results...");
            foreach (var row in result.rows)
            {
                // By design the last column of a result row will contain an error
                // description or null if successful
                var errorColumn = row.Last();
                if (errorColumn != null)
                {
                    // An adapter specific error means that the adapter is expecting this error for some
                    // reason. Mostly because the search criteria did not produce a result page. This might
                    // be because the site produces an alert or similar if e.g., the dates are too close to departure
                    if (adapter.HasAdapterSpecificError(errorColumn))
                    {
                        Log.Debug("Detected adapter specific error - skipping row");

                        // SKIP searchDay
                        continue;
                    }

                    // This exception will be raised when the result row contains an error that the adapter does
                    // not expect. This kind of error is usually a scraper related error, e.g., the scraper couldnt 
                    // find an element or similar. Since this is likely not possible to recover from this - and
                    // the scraper probably needs fixing - throw an exception and cancel run.
                    Log.Fatal("Detected scraper error - aborting: {0}", errorColumn);
                    throw new Exception("Error: " + errorColumn);
                }

                try
                {
                    adapter.CheckForNoNullsAllowedException(row);
                }
                catch (NoNullAllowedException ex)
                {
                    Log.Debug("Detected all nulls in result row - retrying");

                    // this error can be due to a manual cancellation of the execution or some other untimely hangup
                    ExecuteRun(runId, inputLine, adapter);
                    return;
                }

                if (adapter.IsNoFlightAvailable(row))
                {
                    Log.Debug("No flights available - skipping row");

                    // SKIP searchDay
                    continue;
                }

                Log.Debug("Adapting result row into AirDataRow");

                // Adapt the result row into a valid AirDataRow
                var airDataRows = adapter.AdaptResultRows(row);

                try
                {
                    Log.Debug("Attempting data save...");

                    foreach (var airDataRow in airDataRows)
                    {
                        // Attempt datasave
                        _contentExpressRepository.DataSave(airDataRow);
                        rowsInserted++;
                    }
                }
                catch (Exception ex)
                {
                    Log.Fatal(ex.Message);

                    // SOMEKIND OF DB ERROR - LOG OR RETRY?
                    throw ex;
                }
            }
        }
    }
}
