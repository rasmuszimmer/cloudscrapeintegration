﻿using System;

namespace CloudScrapeCC
{
    public static class Utils
    {
        public static DateTime GetDateFromSearchDay(string day)
        {
            var dayAsInt = Int32.Parse(day);
            var today = DateTime.Now;
            return today.AddDays(dayAsInt);
        }

        //public static string Get

        /// <summary>
        /// Method that converts a .NET DateTime to BPT time format
        /// If dt 2000-01-01 00:00:00 then 0 is returned
        /// </summary>
        /// <param name="dt">DateTime parameter</param>
        /// <returns></returns>
        public static int DateTime2BPT(DateTime dt)
        {
            return GetDaysPastY2000(dt)*100000 + GetSecondOfDay(dt);
        }

        /// <summary>
        /// Method that calculates number of days from 2000-01-01 00:00:01 and the DateTime parameter
        /// If dt < 2000-01-01 00:00:01 then 0 is returned
        /// </summary>
        /// <param name="dt">DateTime parameter</param>
        /// <returns></returns>
        private static int GetDaysPastY2000(DateTime dt)
        {
            TimeSpan span = dt - new DateTime(2000, 1, 1);
            return Math.Max(span.Days, 0);
        }

        private static int GetSecondOfDay(DateTime dt)
        {
            return dt.Hour * 60 * 60 + dt.Minute * 60 + dt.Second;
        }
    }
}
