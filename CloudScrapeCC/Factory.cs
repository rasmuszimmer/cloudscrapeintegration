﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudScrapeAPI;
using NLog;

namespace CloudScrapeCC
{
    internal abstract class Factory
    {
        protected Logger Log { get; private set; }
        protected CloudScrape CloudScrape;
        protected static readonly string ApiKey = ConfigurationManager.AppSettings["ApiKey"];
        protected static readonly string AccountId = ConfigurationManager.AppSettings["AccountId"];

        protected Factory()
        {
            // Init CloudScrape API client
            CloudScrape = new CloudScrape();
            CloudScrape.Init(ApiKey, AccountId);
            Log = LogManager.GetLogger(GetType().FullName);
        }

        internal abstract Robot Create();
    }
}
