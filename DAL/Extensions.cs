﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public static class Extensions
    {
        /// <summary>
        /// Converts the value in this <see cref="dataRow"/> and <see cref="dataColumn"/>
        /// into <see cref="T"/>.
        /// This method will perform an actual conversion, if necessary, unlike the Field extension method, which only casts.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataRow"></param>
        /// <param name="dataColumn"></param>
        /// <returns></returns>
        public static T Convert<T>(this DataRow dataRow, DataColumn dataColumn)
        {
            if (dataRow == null) throw new ArgumentNullException("dataRow");
            if (dataColumn == null) throw new ArgumentNullException("dataColumn");

            var value = dataRow[dataColumn];
            if (value == DBNull.Value)
            {
                return default(T);
            }
            var targetType = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
            return (T)System.Convert.ChangeType(value, targetType, CultureInfo.InvariantCulture);
        }
        public static T Convert<T>(this DbDataReader dataReader, int columnOrdinal)
        {
            if (dataReader == null) throw new ArgumentNullException("dataReader");

            var value = dataReader[columnOrdinal];
            if (value == DBNull.Value)
            {
                return default(T);
            }
            var targetType = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
            return (T)System.Convert.ChangeType(value, targetType, CultureInfo.InvariantCulture);
        }
    }
}
