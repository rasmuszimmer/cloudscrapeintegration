﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ContentExpress
{
    
    public class AirDataRow
    {
        #region Properties

        public string TableName { get; set; }
        public string JobId { get; set; }
        public DateTime BptFound { get; set; }
        public string Pos { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Carrier { get; set; }
        public string FirstFlightNo { get; set; }
        public string LastFlightNo { get; set; }
        public string FirstFlightClass { get; set; }
        public string LastFlightClass { get; set; }
        public int BptFirstTravelDep { get; set; }
        public int BptFirstTravelArr { get; set; }
        public int? BptLastTravelDep { get; set; }
        public int? BptLastTravelArr { get; set; }
        public bool IsOneWay { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
        public bool IsTaxIncluded { get; set; }
        public int? Tax { get; set; }
        public int? BookingFee { get; set; }
        public string FirstTravelStopOver { get; set; }
        public string LastTravelStopOver { get; set; }
        public string FareBasisFirst { get; set; }
        public string FareBasisLast { get; set; }
        public string BookingClassFirst { get; set; }
        public string BookingClassLast { get; set; }
        public string FareType { get; set; }
        public int? BptFirstSale { get; set; }
        public int? BptLastSale { get; set; }
        // Seats (Map to AP)
        public string Seats { get; set; }
        public string MinStay { get; set; }
        public string MaxStay { get; set; }
        public string RuleNo { get; set; }
        public string RuleText { get; set; }
        public string Note { get; set; }

        #endregion
    }
}
