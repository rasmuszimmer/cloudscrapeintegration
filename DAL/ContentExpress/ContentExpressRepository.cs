﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NLog;

namespace DAL.ContentExpress
{
    public interface IContentExpressRepository
    {
        void DataSave(AirDataRow row);
    }

    public class ContentExpressRepository : IContentExpressRepository
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly string ConnectionString =
            ConfigurationManager.ConnectionStrings["beclu4"].ConnectionString;
        public void DataSave(AirDataRow row)
        {
            SqlParameter[] sqlParameters =
            {
                new SqlParameter("@table", SqlDbType.NVarChar, 128) {Value = row.TableName},
                new SqlParameter("@jobID", SqlDbType.VarChar, 12) {Value = row.JobId},
                new SqlParameter("@bpt_found", SqlDbType.Int) {Value = DBNull.Value},
                new SqlParameter("@pos", SqlDbType.VarChar, 3) {Value = row.Pos},
                new SqlParameter("@origin", SqlDbType.VarChar, 50) {Value = row.Origin},
                new SqlParameter("@destination", SqlDbType.VarChar, 50) {Value = row.Destination},
                new SqlParameter("@carrier", SqlDbType.VarChar, 4) {Value = row.Carrier},
                new SqlParameter("@first_flight_no", SqlDbType.VarChar, 50) {Value = row.FirstFlightNo},
                new SqlParameter("@last_flight_no", SqlDbType.VarChar, 50) {Value = row.LastFlightNo},
                new SqlParameter("@first_flight_class", SqlDbType.VarChar, 50) {Value = row.FirstFlightClass},
                new SqlParameter("@last_flight_class", SqlDbType.VarChar, 50) {Value = row.LastFlightClass},
                new SqlParameter("@bpt_first_travel_dep", SqlDbType.BigInt) {Value = row.BptFirstTravelDep},
                new SqlParameter("@bpt_first_travel_arr", SqlDbType.BigInt) {Value = row.BptFirstTravelArr},
                new SqlParameter("@bpt_last_travel_dep", SqlDbType.BigInt) {Value = row.BptLastTravelDep },
                new SqlParameter("@bpt_last_travel_arr", SqlDbType.BigInt) {Value = row.BptLastTravelArr},
                new SqlParameter("@one_way", SqlDbType.VarChar, 12) {Value = row.IsOneWay ? "1" : "0"},
                new SqlParameter("@price", SqlDbType.Money) {Value = row.Price},
                new SqlParameter("@currency", SqlDbType.VarChar, 5) {Value = row.Currency},
                new SqlParameter("@tax_inc", SqlDbType.VarChar, 5) {Value = row.IsTaxIncluded ? "1" : "0"},
                new SqlParameter("@tax", SqlDbType.Money) {Value = row.Tax},
                new SqlParameter("@booking_fee", SqlDbType.Money) {Value = row.BookingFee},
                new SqlParameter("@first_travel_stop_over", SqlDbType.VarChar, 50) {Value = row.FirstTravelStopOver},
                new SqlParameter("@last_travel_stop_over", SqlDbType.VarChar, 50) {Value = row.LastTravelStopOver },
                new SqlParameter("@fare_basis_first", SqlDbType.VarChar, 50) {Value = row.FareBasisFirst},
                new SqlParameter("@fare_basis_last", SqlDbType.VarChar, 50) {Value = row.FareBasisLast},
                new SqlParameter("@booking_class_first", SqlDbType.VarChar, 5) {Value = row.BookingClassFirst},
                new SqlParameter("@booking_class_last", SqlDbType.VarChar, 5) {Value = row.BookingClassLast},
                new SqlParameter("@fare_type", SqlDbType.VarChar, 50) {Value = row.FareType},
                new SqlParameter("@bpt_first_sale", SqlDbType.BigInt) {Value = row.BptFirstSale},
                new SqlParameter("@bpt_last_sale", SqlDbType.BigInt) {Value = row.BptLastSale},
                // Seats mapped to AP for historical reasons
                new SqlParameter("@advance_purchase", SqlDbType.VarChar, 50) {Value = row.Seats},
                new SqlParameter("@min_stay", SqlDbType.VarChar, 50) {Value = row.MinStay},
                new SqlParameter("@max_stay", SqlDbType.VarChar, 50) {Value = row.MaxStay},
                new SqlParameter("@rule_no", SqlDbType.VarChar, 50) {Value = row.RuleNo},
                new SqlParameter("@rule_text", SqlDbType.VarChar, 2500) {Value = row.RuleText},
                new SqlParameter("@note", SqlDbType.VarChar, 500) {Value = row.Note}

            };


            using (var connection = new SqlConnection(ConnectionString))
            using (var cmd = new SqlCommand("usp_generic_insert_air", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 30;
                cmd.Parameters.AddRange(sqlParameters);

                _logger.Debug("Opening connection: {0}", ConnectionString);
                connection.Open();

                _logger.Debug("Executing: {0}", "usp_generic_insert_air");
                cmd.ExecuteNonQuery();
            }
        }
    }
}
