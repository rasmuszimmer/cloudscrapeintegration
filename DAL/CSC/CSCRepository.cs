﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NLog;

namespace DAL.CSC
{
    public interface ICSCRepository
    {
        List<SearchCriteria> GetSearchCriteria(int filterId);
    }

    public class CSCRepository : ICSCRepository
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["CSC"].ConnectionString;
        private readonly SearchCriteriaFactory _factory = new SearchCriteriaFactory();

        public List<SearchCriteria> GetSearchCriteria(int filterId)
        {
            _logger.Debug("Getting search criteria for filter: {0}", filterId);

            var criteria = new List<SearchCriteria>();

            SqlParameter[] sqlParameters =
            {
                new SqlParameter("@filter_id", SqlDbType.Int) {Value = filterId},
                new SqlParameter("@instance_id", SqlDbType.Int) {Value = 0},
            };

            using (var connection = new SqlConnection(_connectionString))
            using (var cmd = new SqlCommand("Main.usp_csc_get_filter_search_criteria", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 30;
                cmd.Parameters.AddRange(sqlParameters);

                _logger.Debug("Opening connection: {0}", _connectionString);

                connection.Open();

                _logger.Info("Executing: {0} with params {1} and {2}", "Main.usp_csc_get_filter_search_criteria", filterId, 0);
                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        var c = _factory.CreateSingleFrom(rdr.GetString(0));
                        criteria.Add(c); 
                    }
                }

                return criteria;
            }
        }
    }
}
