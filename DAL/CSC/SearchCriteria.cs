﻿using System;
using System.Data.Common;
using System.Globalization;

namespace DAL.CSC
{
    public class SearchCriteria
    {
        public string OutDay { get; set; }
        public string OutMonth { get; set; }
        public string OutYear { get; set; }
        public string HomeDay { get; set; }
        public string HomeMonth { get; set; }
        public string HomeYear { get; set; }
        public string Pos { get; set; }
        public string Origin { get; set; }
        public string Poa { get; set; }
        public string Destination { get; set; }
        public string Cabin { get; set; }
        public string AllowedConnections { get; set; }
        public string AllowedCarriers { get; set; }
        public string Wildcard { get; set; }
        public string MainLeg { get; set; }
        public string Adults { get; set; }
        public string Children { get; set; }
        public string Infants { get; set; }
        public string RowId { get; set; }
    }
}
