﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.CSC
{
    public interface ISearchCriteriaFactory
    {
        SearchCriteria CreateSingleFrom(string searchCriteriaLine);
    }

    public class SearchCriteriaFactory : ISearchCriteriaFactory
    {
        public SearchCriteria CreateSingleFrom(string searchCriteriaLine)
        {
            var parameters = searchCriteriaLine.Split("|".ToCharArray());
            return new SearchCriteria()
            {
                OutDay = parameters[0],
                OutMonth = parameters[1],
                OutYear = parameters[2],
                HomeDay = parameters[3],
                HomeMonth = parameters[4],
                HomeYear = parameters[5],
                Pos = parameters[6],
                Origin = parameters[7],
                Poa = parameters[8],
                Destination = parameters[9],
                Cabin = parameters[10],
                AllowedConnections = parameters[11],
                AllowedCarriers = parameters[12],
                Wildcard = parameters[13],
                MainLeg = parameters[14],
                Adults = parameters[15],
                Children = parameters[16],
                Infants = parameters[17],
                RowId = parameters[18]
            };
        }
    }
}
