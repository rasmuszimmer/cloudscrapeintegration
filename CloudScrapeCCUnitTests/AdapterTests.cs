﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudScrapeCC.Adapters;
using CloudScrapeCC.Adapters.AwanSqivaCom;
using DAL.CSC;
using NUnit.Core;
using NUnit.Framework;

namespace CloudScrapeCCUnitTests
{
    public class AdapterTests
    {
        public class SkyjetOwTests
        {
            public class InputFormatterTests
            {
                [TestCase("MNL", "MNL")]
                [TestCase("USU", "USU")]
                public void InputFormatter_ShouldFormatOrigin(string input, string expected)
                {
                    var inputFormatter = new SkyjetOwAdapter.SkyjetOwInputFormatter();
                    var formattedInput = inputFormatter.FormatOrigin(input);

                    Assert.That(formattedInput, Is.EqualTo(expected));
                }

                [TestCase("MNL", "MNL")]
                [TestCase("USU", "USU")]
                public void InputFormatter_ShouldFormatDestination(string input, string expected)
                {
                    var inputFormatter = new SkyjetOwAdapter.SkyjetOwInputFormatter();
                    var formattedInput = inputFormatter.FormatDestination(input);

                    Assert.That(formattedInput, Is.EqualTo(expected));
                }

                [TestCase("01", "1")]
                [TestCase("02", "2")]
                [TestCase("03", "3")]
                [TestCase("04", "4")]
                [TestCase("05", "5")]
                [TestCase("13", "13")]
                [TestCase("25", "25")]
                [TestCase("31", "31")]
                public void InputFormatter_ShouldFormatDay(string input, string output)
                {
                    var inputFormatter = new SkyjetOwAdapter.SkyjetOwInputFormatter();
                    var formattedInput = inputFormatter.FormatDay(input);

                    StringAssert.AreEqualIgnoringCase(output, formattedInput);
                }

                [TestCase("1", "0")]
                [TestCase("2", "1")]
                [TestCase("3", "2")]
                [TestCase("12", "11")]
                public void InputFormatter_ShouldFormatMonth(string input, string output)
                {
                    var inputFormatter = new SkyjetOwAdapter.SkyjetOwInputFormatter();
                    var formattedInput = inputFormatter.FormatMonth(input);

                    StringAssert.AreEqualIgnoringCase(output, formattedInput);
                }

                [TestCase("2015", "2015")]
                [TestCase("2016", "2016")]
                public void InputFormatter_ShouldFormatYear(string input, string output)
                {
                    var inputFormatter = new SkyjetOwAdapter.SkyjetOwInputFormatter();
                    var formattedInput = inputFormatter.FormatYear(input);

                    StringAssert.AreEqualIgnoringCase(output, formattedInput);
                }
            }

            public class OutputFormatterTests
            {
                [TestCase(1782, "M8", "1782M8")]
                [TestCase(7368, "4P", "73684P")]
                [TestCase(2345.22, "4P", "23454P")]
                public void OutputFormatter_ShouldFormatFareBasis(double price, string carrier, string expected)
                {
                    var outputFormatter = new SkyjetOwAdapter.SkyjetOwOutputFormatter();
                    var formattedOutput = outputFormatter.FormatFareBasis(price, carrier);

                    Assert.That(formattedOutput, Is.EqualTo(expected));
                }

                [TestCase("M8-1234", "M8")]
                [TestCase("4P-982", "4P")]
                public void OutputFormatter_ShouldFormatCarrier(string input, string expected)
                {
                    var outputFormatter = new SkyjetOwAdapter.SkyjetOwOutputFormatter();
                    var formattedOutput = outputFormatter.FormatCarrier(input);

                    Assert.That(formattedOutput, Is.EqualTo(expected));
                }

                [TestCase("M8-1234", "M81234")]
                [TestCase("4P-981", "4P981")]
                public void OutputFormatter_ShouldFormatFlightNo(string input, string expected)
                {
                    var outputFormatter = new SkyjetOwAdapter.SkyjetOwOutputFormatter();
                    var formattedOutput = outputFormatter.FormatFlightNo(input);

                    Assert.That(formattedOutput, Is.EqualTo(expected));
                }

                [TestCase("6", "11", "2015", "1030 (MNL)", 581837800)]
                [TestCase("6", "11", "2015", "1110 (MNL)", 581840200)]
                public void OutputFormatter_ShouldFormatBpt(string day, string month, string year, string time, int expected)
                {
                    var outputFormatter = new SkyjetOwAdapter.SkyjetOwOutputFormatter();
                    var formattedOutput = outputFormatter.FormatBpt(day, month, year, time);

                    Assert.AreEqual(expected, formattedOutput);
                }

                [TestCase("2249 (MNL)", new []{22, 49})]
                [TestCase("1030 (MNL)", new[] { 10, 30 })]
                [TestCase("0010 (MNL)", new[] { 0, 10 })]
                public void OutputFormatter_ShouldExtractTime(string input, int[] expected)
                {
                    var outputFormatter = new SkyjetOwAdapter.SkyjetOwOutputFormatter();
                    var formattedOutput = outputFormatter.ExtractTime(input);

                    Assert.AreEqual(expected, formattedOutput);
                }

                [TestCase("PHP 1,337.28++", 1337.28)]
                [TestCase("PHP 1,337++", 1337)]
                [TestCase("PHP 10,337.12++", 10337.12)]
                public void OutputFormatter_ShouldFormatPrice(string input, double expected)
                {
                    var outputFormatter = new SkyjetOwAdapter.SkyjetOwOutputFormatter();
                    var formattedOutput = outputFormatter.FormatPrice(input);

                    Assert.AreEqual(expected, formattedOutput);
                }

                [TestCase("PHP 1,337.28++", "PHP")]
                [TestCase("DKK 1,337++", "DKK")]
                [TestCase("USD 10,337.12++", "USD")]
                public void OutputFormatter_ShouldFormatCurrency(string input, string expected)
                {
                    var outputFormatter = new SkyjetOwAdapter.SkyjetOwOutputFormatter();
                    var formattedOutput = outputFormatter.FormatCurrency(input);

                    Assert.That(formattedOutput, Is.EqualTo(expected));
                }
            }
        }

        public class TravelAirOwTests
        {
            [TestCase("Assertion failed - element not found")]
            public void Adapter_ShouldRecognizeSpecificError(string input)
            {
                // Test input
                //string[] row = { "WWK", "LAE", "DEC 2015", "6", null, null, null, null, "Assertion failed - element not found" };

                var adapter = new TravelAirOwAdapter("10070295");
                Assert.True(adapter.HasAdapterSpecificError(input));
            }

            [Test]
            public void Adapter_ShouldRecognizeNoFlightRow()
            {
                // Test input
                string[] row = { "WWK", "LAE", "DEC 2015", "6", "Flight No.", "Depart", "Arrival", "Fare", null };

                var adapter = new TravelAirOwAdapter("10070295");
                Assert.True(adapter.IsNoFlightAvailable(row));
            }
        }

        public class DeltaOwTests
        {
            [TestCase("12072015", "7:10AM", 581925800)]
            [TestCase("12132015", "7:10AM", 582525800)]
            public void OutputFormatter_ShouldFormatBpt(string date, string time, int expected)
            {
                var outputFormatter = new DeltaOwAdapter.DeltaOwOutputFormatter();
                var formattedOutput = outputFormatter.FormatBpt(date, time);

                Assert.AreEqual(expected, formattedOutput);
            }

            [TestCase("$656.60    ", 656.60)]
            [TestCase("$663.10    ", 663.10)]
            [TestCase("$1,282.60    ", 1282.60)]
            public void OutputFormatter_ShouldFormatPrice(string price, double expected)
            {
                var outputFormatter = new DeltaOwAdapter.DeltaOwOutputFormatter();
                var formattedOutput = outputFormatter.FormatPrice(price);

                Assert.AreEqual(expected, formattedOutput);
            }

            [TestCase(656.34, "DL", "656DL")]
            [TestCase(2656, "DL", "2656DL")]
            public void OutputFormatter_ShouldFormatFareBasis(double price, string carrier, string expected)
            {
                var outputFormatter = new DeltaOwAdapter.DeltaOwOutputFormatter();
                var formattedOutput = outputFormatter.FormatFareBasis(price, carrier);

                Assert.That(formattedOutput, Is.EqualTo(expected));
            }
        }
    }
}
